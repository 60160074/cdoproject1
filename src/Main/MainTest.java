/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Data.Book;
import Data.Borrow;
import Data.Data;
import Data.Log;
import Data.User;
import dao.BookDao;
import dao.BorrowDao;
import static dao.BorrowDao.getBorrowSelect;
import dao.LogDao;
import dao.UserDao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author c3356
 */
public class MainTest {

    /**
     * @param args the command line arguments
     */
    public static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {

        showHello();
        int loadData = 0;

        if (loginSystem(loadData)) {
            while (true) {
                if (checkUserStatus()) {
                    if (checkInputMenuAdmin()) break;
                } else {
                    if (checkInputMenuMember()) break;
                    
                }
            }
        }
    }

    private static boolean checkInputMenuMember() throws SQLException {
        showUserMenuScreen();
   
                
        int g = inPutMenu();
           if (g == 1) {
            showUserforMember();
        } else if (g == 2) {
            selectBorrowBook();
          
        }else if (g == 3) {
          int  load = 0;
          if(load==0){
              loadUserBorrow();
              load++;
          }
            showBorrow();
        }else if (g == 4) {
            
            System.out.println("Log out  goodbye " + getUserStatus());
            return true;
        }
        return false;
    }

    private static boolean checkInputMenuAdmin() throws SQLException {
        showAdminMenuScreen();
        int g = inPutMenu();
        if (g == 2) {
            showUserforAdmin();
        } else if (g == 1) {
            insertUserData();
        } else if (g == 3) {
            deleteUser();
        } else if (g == 4) {
            showLogHistory();
        } else if (g == 5) {
             seeShelf();
        } else if (g == 6) {
           addbooktoShelf();
        }else if (g == 7) {
        removeBookonShelf();
            
        }else if (g == 8) {
          seeUserBorrow();
        }else if (g == 9) {
            usertoEdit();
        } else if (g == 10) {
            System.out.println("Log out  goodbye " + getUserStatus());
            return true;
        } else {
            checkInputMenuAdmin();
        }
        return false;
    }

    private static void showLogHistory() {
        System.out.println("Log History");
        int num = 0;
        ArrayList<Log> list = Data.loglist;
        System.out.println("Logid\tDate&Time\t\tUserID");
        for (Log log : list) {
            num++;
            System.out.println(num + "\t" + log.getDatetime() + "\t" + log.getUserid());
        }
        back();
    }

    private static String getUserStatus() {
        return LogDao.getLogStatus();
    }

    private static boolean checkUserStatus() {
        return getUserStatus().equals("Admin");
    }

    private static boolean loginSystem(int loadData) throws SQLException {
        ArrayList<User> list = Data.userlist;
        loadData(loadData);
        System.out.println("Login Page");
        System.out.println("input username :");
        String user = kb.next();
        System.out.println("input password :");
        String password = kb.next();
        return checkUser(list, user, password);
    }

    private static void loadData(int loadData) throws SQLException {
        checkLoadData(loadData);
        loadData++;
    }

    private static boolean checkUser(ArrayList<User> list, String user, String password) throws SQLException {
        boolean check =true;
       
         
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUsername().equals(user)) {
                if (list.get(i).getPassword().equals(password)) {
                    System.out.println("Login complete");
                    String id = String.valueOf(list.get(i).getUserId());
                    getcurrentlogin(list, i);
                    int type = list.get(i).getTypeId();
                    saveLog(id);
                    checkStatus(type);
                     check = false;
                    return true;
                   

                }
                 
       
                
            
            
                 
               
                
            }
        }
    
        return false;
    }

    private static void getcurrentlogin(ArrayList<User> list, int i) {
        LogDao.currentlogin =list.get(i).getUserId();
    }

    private static void saveLog(String id) throws SQLException {
        LogDao.saveLog(id);
    }

    private static void checkStatus(int type) {
        LogDao.setLogStatus(type);
    }

    private static void checkLoadData(int loadData) throws SQLException {

        if (loadData == 0) {
            loadUser();
            loadLog();
            loadBookShelf();
            loadBorrowList();
        
      
        }
    }

    /**
     *
     */
    private static void showHello() {
        System.out.println("Welcome to test data base Please choose your choice");

    }

    private static int inPutMenu() {
        Scanner key = new Scanner(System.in);
        System.out.print("Select Menu :");
        try{
        int choose = key.nextInt();
        return choose;
        }catch(Exception e){
            System.out.println("Input Must have in choise");
            inPutMenu();
        }
        return 0;
        
    }

    private static void insertUserData() throws SQLException {
try{
        System.out.println("Username:");
        String Username = kb.next();
        System.out.println("Password:");
        String password = kb.next();
        System.out.println("Name:");
        String name = kb.next();
        System.out.println("SurName:");
        String surname = kb.next();
        System.out.println("TypeId:");
        int typeId = kb.nextInt();
        System.out.println("Weight:");
        int weight = kb.nextInt();
        System.out.println("Height:");
        int height = kb.nextInt();
        UserDao.saveUser(Username, password, name, surname, typeId, weight, height);
}catch(Exception e){
      System.out.println("Check your type of input ");
  
}
    }    private static void usertoEdit() throws SQLException {
try{ 
       System.out.println("Welcome to Edit");
       showUser();
        System.out.println("Select userId to Edit:");
         int  userid = kb.nextInt();
        System.out.println("Edit wit User id :"+userid);
        System.out.println("Username:");
        String username = kb.next();
        System.out.println("Password:");
        String password = kb.next();
        System.out.println("Name:");
        String name = kb.next();
        System.out.println("SurName:");
        String surname = kb.next();
        System.out.println("TypeId:");
        int typeId = kb.nextInt();
        System.out.println("Weight:");
        int weight = kb.nextInt();
        System.out.println("Height:");
        int height = kb.nextInt();
        UserDao.UpdateUser(username, password, name, surname, typeId, weight, height,userid);
}catch(Exception e){
      System.out.println("Check your type of input ");
  
}
    }

    private static void deleteUser() throws SQLException {
        showUsertoDel();
        System.out.println("Selecrt Username to delete:");
        String del = kb.next();
        UserDao.deleteUser(del);

    }

    public static void loadUser() throws SQLException {

        UserDao.getUser();

    }

    public static void showUserforMember() throws SQLException {
   System.out.println("\t\tUser Screen\t\t");

        ArrayList<User> list = Data.userlist;
        for (User user : list) {
         
              System.out.println("UserName\t\tUserId");
            System.out.println( user.getLogin()+"\t\t"+user.getUserId());
        }
        back();
    }  public static void showUser() throws SQLException {
   System.out.println("\t\tUser Screen\t\t");

        ArrayList<User> list = Data.userlist;
        for (User user : list) {
         
              System.out.println("UserName\t\tUserId");
            System.out.println( user.getLogin()+"\t\t\t\t"+user.getUserId());
        }
       
    }

    private static void back() {
        System.out.println("press type back to back");
        String back = kb.next();
        if (back.equals("back")) {

        } else {
            System.out.println("press Type " + "back");
            back();
        }
    }

    public static void showUserforAdmin() throws SQLException {
        System.out.println("\t\tUser Screen\t\t");

        ArrayList<User> list = Data.userlist;
        for (User user : list) {
         
              System.out.println("[UserName]\t[TypeId]\t[Password]");
            System.out.println( user.getLogin()+"\t\t"+user.getTypeId()+"\t\t"+user.getPassword());
        }
        back();
    }

    private static void showAdminMenuScreen() {

        System.out.println("Welcome Admin to Menu");
        System.out.println("1. Add Users");
        System.out.println("2. Show Users");
        System.out.println("3. Delete Users");
        System.out.println("4. ShowLog History");
        System.out.println("5. See Book on Shelf");
        System.out.println("6. Add Book to Shelf");
        System.out.println("7. Remove Book of Shelf");
        System.out.println("8. See User Borrow");
        System.out.println("9. Edit User ");
        System.out.println("10. logout ");

    }

    private static void showUserMenuScreen() {

        System.out.println("Welcome MemBer to Menu");
        System.out.println("1. Show Users");
        System.out.println("2. BrrowBook");
         System.out.println("3.Show Your Borrow");
        System.out.println("4.Logout");

    }

    public static void showUsertoDel() throws SQLException {
        System.out.println("Delete User");
        int num = 0;
        ArrayList<User> list = Data.userlist;
        for (User user : list) {
            num++;
         System.out.println(num + " " + user.getLogin()+" "+user.getUserId());
        }

    }

    private static void loadLog() throws SQLException {
        LogDao.getLog();
    }private static void loadBookShelf() throws SQLException {
        BookDao.getBook();
    }

    private static void seeShelf() {
   System.out.println("\t\t\t"+"BookShelf"+"\t\t\t");

        ArrayList<Book> list = Data.booklist;
        System.out.println("[BookName]\t\t\t[ BookId]");
        for (Book book : list) { 
            System.out.println( book.getBookname()+" \t\t\t"+book.getId());
        }
        back();
    }   private static void showShelf() {
   System.out.println("        BookShelf   ");
   ArrayList<Book> list = Data.booklist;
   int user   =LogDao.currentlogin;
        System.out.println("Member id : "+user+"  number of book in your list:"+Data.borrowlist1.size());
       
        System.out.println("[BookName]     [ BookId]    ");
        for (Book book : list) { 
            System.out.println( book.getBookname()+"        "+book.getId());
        }
   
    } private static void showBorrow() {
   System.out.println("        BookShelf   ");
   ArrayList<Borrow> list = Data.borrowlist1;
   int user   =LogDao.currentlogin;
        System.out.println("Member id : "+user);
       
        System.out.println("[BookId]\t\t[ TimeDate]\t\t");
        for (Borrow borrow : list) { 
            System.out.println( borrow.getBookid()+"\t\t "+borrow.getBorrowtime());
        }
   
    }

    private static void addbooktoShelf() throws SQLException {
  System.out.print("Insert name of book to add :");
        String name  = kb.next();
        BookDao.insertBook(name);
    }

    private static void removeBookonShelf()throws SQLException {
   seeShelf();
   System.out.print("Insert name of book to remove:");
        String name  = kb.next();
        BookDao.removeBook(name);

    }

    private static void loadBorrowList() throws SQLException {
   BorrowDao.getBorrow();
    }

    private static void seeUserBorrow() {
  System.out.println("\t\t\t[BorrowList]\t\t\t");

        ArrayList<Borrow> list = Data.borrowlist;
      
        System.out.println("[Userid]\t\t[ BookId]\t\t[DateTime]  ");
        for (Borrow borrow : list) { 
            System.out.println(borrow.getUserid()+"\t\t\t"+borrow.getBookid()+"\t\t\t"+borrow.getBorrowtime());
        }
        back();
    }
public static void selectBorrowBook() throws SQLException{
       showShelf();
         System.out.println("Select Book Number :");
       int  bookid  = kb.nextInt();
         BorrowDao.addBorrow(bookid);
        
        
    }

    private static void loadUserBorrow() throws SQLException {
    int  id = LogDao.getCurrentlogin();
  
  
  
        BorrowDao.getBorrowSelect(id);
    }

    

}
