/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Data.Data;
import Data.Db;
import Data.Log;
import Data.User;
import static dao.UserDao.currentid;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author c3356
 */
public class LogDao {
  static String current;
  public static int currentlogin;

    public static int getCurrentlogin() {
        return currentlogin;
    }

    public static void setCurrentlogin(int currentlogin) {
        LogDao.currentlogin = currentlogin;
    }
  
    public static ArrayList<Log> getLog() throws SQLException {
      
        ArrayList<Log> list = Data.loglist;
  
        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "SELECT logID,\n" +
"       dateTime,\n" +
"       userId\n" +
"  FROM log\n" 
;
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

              Log log  = ToObject(rs);
              list.add(log);
            }

           
            Db.disConnect();
            return list;

        } catch (SQLException ex) {

        }
        return list;
    } public static void saveLog(String userid) throws SQLException {

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "INSERT INTO log (\n" +
"                    userId\n" +
"                )\n" +
"                VALUES (\n" +userid +
"                );" ;    stm.execute(sql);
           
Db.disConnect();

        } catch (SQLException ex) {
            System.out.print(ex);
        }
    }

    private static  Log  ToObject(ResultSet rs) throws SQLException {
        Log  log  =  new Log();
        log.setDatetime(rs.getString("dateTime"));
        log.setUserid(rs.getString("userId"));
        return log;
    }public static void  setLogStatus(int currentlog){
    
        if(currentlog==0){
           current = "Admin"; 
        }else{
           current = "member"; 
        }
    }public static String   getLogStatus(){
     return current;
    }
}
