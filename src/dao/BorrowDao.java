/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Data.Borrow;
import Data.Book;
import Data.Data;
import Data.Db;
import Data.Log;
import Data.User;
import static dao.UserDao.currentid;
import static dao.UserDao.kb;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author c3356
 */
public class BorrowDao {

    static String current;

    public static ArrayList<Borrow> getBorrow() throws SQLException {

        ArrayList<Borrow> list = Data.borrowlist;

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "SELECT borrowid,\n"
                    + "       userId,\n"
                    + "       bookId,\n"
                    + "       borrowTime\n"
                    + "  FROM bowrow;";
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

                Borrow borrow = ToObject(rs);
                list.add(borrow);
            }

            Db.disConnect();

            return list;

        } catch (SQLException ex) {

        }
        return list;
    }

    public static void addBorrow(int bookid) throws SQLException {

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            int userid = findUserborrow();
            String sql = "INSERT INTO bowrow (\n"
                    + "                       userId,\n"
                    + "                       bookId\n"
                    + "                   )\n"
                    + "                   VALUES (\n"
                    + "                       '" + userid + "',\n"
                    + "                       '" + bookid + "'\n"
                    + "                   );";
            stm.execute(sql);

            System.out.println("Borrow Success");
            Db.disConnect();
        } catch (SQLException ex) {
            System.out.print(ex);
        }
    }

    private static Borrow ToObject(ResultSet rs) throws SQLException {
        Borrow borrow = new Borrow();
        borrow.setBrrowid(rs.getInt("borrowid"));

        borrow.setUserid((rs.getInt("userId")));
        borrow.setBookid((rs.getInt("bookId")));
        borrow.setBorrowtime(rs.getString("borrowTime"));
        return borrow;
    }

    private static int findUserborrow() {
        int userid = LogDao.currentlogin;
        return userid;
    }

    public static ArrayList<Borrow> getBorrowSelect(int id) throws SQLException {

        ArrayList<Borrow> list = Data.borrowlist1;

        try {
        
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "SELECT borrowid,\n" +
"       userId,\n" +
"       bookId,\n" +
"       borrowTime\n" +
"  FROM bowrow\n" +
" WHERE userId = '"+id+"';";
            
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

                Borrow borrow = ToObject(rs);
                list.add(borrow);
            }

            Db.disConnect();

            return list;

        } catch (SQLException ex) {
System.out.print(ex);
        }
        return list;
    }
}
